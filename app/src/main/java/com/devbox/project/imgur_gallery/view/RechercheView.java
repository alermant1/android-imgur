package com.devbox.project.imgur_gallery.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.devbox.project.imgur_gallery.R;

/**
 * Created by alex on 17/11/17.
 */

/**
 * Permet d'afficher le bloc de Recherche
 */
public class RechercheView extends LinearLayout {

    public RechercheView(Context context) {
        super(context);
    }

    public RechercheView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        initView(context, attrs);
    }

    private void initView(final Context context, AttributeSet attrs) {

        final View view = LayoutInflater.from(context)
                .inflate(R.layout.recherche, this, true);

        final TypedArray array = context.obtainStyledAttributes(
                attrs,
                R.styleable.RechercheView,
                0,
                0
        );

        final EditText editRecherche = view.findViewById(R.id.editText);

        editRecherche.setHint(array.getText(R.styleable.RechercheView_placeholder));

        array.recycle();
    }
}
