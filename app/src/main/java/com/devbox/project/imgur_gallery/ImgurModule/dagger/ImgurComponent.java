package com.devbox.project.imgur_gallery.ImgurModule.dagger;

import com.devbox.project.imgur_gallery.activity.DetailsActivity;
import com.devbox.project.imgur_gallery.activity.MainActivity;

import dagger.Subcomponent;

/**
 * Created by alex on 17/11/17.
 */

@Subcomponent(modules = ImgurModule.class)
public interface ImgurComponent {
    void inject (MainActivity activity);
    void injectDetail (DetailsActivity activity);
}
