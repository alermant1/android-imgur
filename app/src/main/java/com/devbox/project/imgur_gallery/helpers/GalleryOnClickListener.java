package com.devbox.project.imgur_gallery.helpers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.devbox.project.imgur_gallery.activity.DetailsActivity;
import com.devbox.project.imgur_gallery.activity.MainActivity;
import com.devbox.project.imgur_gallery.viewmodel.GalleryAdapter;
import com.devbox.project.imgur_gallery.viewmodel.SingleGalleryViewModel;

import static android.content.Intent.FLAG_ACTIVITY_REORDER_TO_FRONT;

/**
 * Created by xps on 17/11/17.
 */

/**
 * Sur mobile quand on click sur un item on affiche la vue sur une nouvelle activity
 */
public class GalleryOnClickListener implements View.OnClickListener {
    private SingleGalleryViewModel galleryViewModel;
    private AppCompatActivity context;


    public GalleryOnClickListener(Context context, SingleGalleryViewModel galleryViewModel) {
        this.galleryViewModel = galleryViewModel;
        this.context = (AppCompatActivity) context;
    }

    @Override
    public void onClick(View view) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("hash", galleryViewModel.getId());
        Intent intent = new Intent(context, DetailsActivity.class);
        intent.putExtras(bundle);
        context.startActivityForResult(intent, 1);
    }
}
