package com.devbox.project.imgur_gallery.ImgurModule.service;

import com.devbox.project.imgur_gallery.ImgurModule.exception.ImgurException;
import com.devbox.project.imgur_gallery.ImgurModule.exception.ImgurNotFoundException;
import com.devbox.project.imgur_gallery.model.DataGetGalleryResponse;
import com.devbox.project.imgur_gallery.model.DataSearchGalleryResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by alex on 17/11/17.
 */

public interface ImgurService {

    final static String AUTH_KEY = "Client-ID 78960fe5f6c7575";

    @GET("gallery/search/")
    @Headers("Authorization: " + AUTH_KEY)
    Call<DataSearchGalleryResponse> searchGallery(@Query("q") String text) throws ImgurNotFoundException, ImgurException;

    @GET("album/{id}")
    @Headers("Authorization: " + AUTH_KEY)
    Call<DataGetGalleryResponse> getGalleryByAlbumId(@Path("id") String hash) throws ImgurNotFoundException, ImgurException;

}

