package com.devbox.project.imgur_gallery.viewmodel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.devbox.project.imgur_gallery.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by alex on 18/11/17.
 */

/**
 * Utilisé pour afficher la gridview d'image pour une gallerie donné
 */
public class DetailGalleryAdapter extends BaseAdapter {

    private final Context context;
    private final SingleGalleryViewModel listItem;

    public DetailGalleryAdapter(Context context, SingleGalleryViewModel listItem) {
        this.context = context;
        this.listItem = listItem;
    }

    @Override
    public int getCount() {
        return listItem.getImages().size();
    }

    @Override
    public Object getItem(int i) {
        return listItem.getImages().get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    /**
     * Permet de générer la liste en "détail", on affiches toutes les images
     * d'une gallerie
     * @param i
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int i, View convertView, ViewGroup parent) {

        ImageView imageView;
        if (convertView == null) {
            imageView = new ImageView(this.context);
            imageView.setLayoutParams(
                    new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT
                    )
            );
            imageView.setPadding(0, 0, 0, 0);
        } else {
            imageView = (ImageView) convertView;
        }

        Picasso.with(context)
                .load(listItem.getImages().get(i).getLink())
                .placeholder(R.drawable.loading)
                .into(imageView);
        imageView.setAdjustViewBounds(true);

        return imageView;

    }
}
