package com.devbox.project.imgur_gallery.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.devbox.project.imgur_gallery.ImgurModule.dagger.ImgurModule;
import com.devbox.project.imgur_gallery.ImgurModule.presenter.ImgurPresenter;
import com.devbox.project.imgur_gallery.R;
import com.devbox.project.imgur_gallery.dagger.DaggerMainComponent;
import com.devbox.project.imgur_gallery.helpers.DetailAnimator;
import com.devbox.project.imgur_gallery.helpers.Toaster;
import com.devbox.project.imgur_gallery.viewmodel.DetailGalleryAdapter;
import com.devbox.project.imgur_gallery.viewmodel.GalleryAdapter;
import com.devbox.project.imgur_gallery.viewmodel.GalleryViewModel;
import com.devbox.project.imgur_gallery.viewmodel.SingleGalleryViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by alex on 17/11/17.
 */

public class MainActivity extends BaseActivity implements ImgurView {

    @BindView(R.id.gridView)
    GridView gridView;

    @BindView(R.id.editText)
    EditText editText;

    @BindView(R.id.validate)
    ImageButton validate;

    @Inject
    ImgurPresenter presenter;

    Toaster toaster;

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.context = this;
        ButterKnife.bind(this);
        DaggerMainComponent.builder()
                .build()
                .plus(new ImgurModule(this))
                .inject(this);

        toaster = new Toaster(this);

        // par default on affiche les meme
        editText.setText("meme");
        presenter.load(editText.getText().toString());

        initView();
    }

    /**
     * Au click du bouton rechercher on execute la recherche
     * au WS
     */
    private void initView() {
        validate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.load(editText.getText().toString());
            }
        });
    }

    public ImgurPresenter getPresenter() {
        return presenter;
    }


    /**
     *  Callback de l'appel au WS pour chercher des galleries
     *  permet d'afficher dans la gridview les images/items
     * @param viewModel
     */
    @Override
    public void displayGallery(GalleryViewModel viewModel) {
        List<SingleGalleryViewModel> list = new ArrayList<SingleGalleryViewModel>();

        // on génère la liste de SingleGalleryViewModel
        for(int counter = 0; viewModel.getAllGallery().size() > counter; counter++) {
            list.add(viewModel.getAllGallery().get(counter));
        }

        toaster.toast(list.size() + " " +getResources().getString(R.string.resultsFound));

        gridView.setAdapter(new GalleryAdapter(this, list));
    }

    @Override
    public void displayNotFound() {
        toaster.toast(getResources().getString(R.string.searchNotFound));
    }

    @Override
    public void displayError() {
        toaster.toast(getResources().getString(R.string.ImgurError));
    }

    /**
     * Appelé quand on a finit de charger une gallery
     * @param viewModel
     */
    @Override
    public void displaySingleGallery(final SingleGalleryViewModel viewModel) {

        final View linearLayout = this.findViewById(R.id.itemDetail);

        final DetailAnimator detailAnimator = new DetailAnimator(linearLayout);

        // on lance l'animation,
        // une fois qu'elle est terminé on appel
        // le callback Runnable pour afficher
        // la nouvelle liste
        detailAnimator.hideDetail(new Runnable() {
            @Override
            public void run() {
                TextView titreDetail = linearLayout.findViewById(R.id.titreDetail);
                TextView auteurDetail = linearLayout.findViewById(R.id.auteurDetail);
                TextView dateDetail = linearLayout.findViewById(R.id.dateDetail);
                TextView descriptionDetail = linearLayout.findViewById(R.id.descriptionDetail);
                GridView gridView = linearLayout.findViewById(R.id.gridDetailView);

                titreDetail.setText(viewModel.getTitle());
                auteurDetail.setText(getResources().getString(R.string.postedBy) + " " + viewModel.getAccountName());
                dateDetail.setText(viewModel.getDateTime());
                descriptionDetail.setText(viewModel.getDescription());

                gridView.setAdapter(new DetailGalleryAdapter(context, viewModel));
                detailAnimator.showDetail();
            }
        });

    }

}
