package com.devbox.project.imgur_gallery.ImgurModule.presenter;

import com.devbox.project.imgur_gallery.ImgurModule.exception.ImgurException;
import com.devbox.project.imgur_gallery.ImgurModule.exception.ImgurNotFoundException;
import com.devbox.project.imgur_gallery.ImgurModule.interactor.ImgurInteractor;
import com.devbox.project.imgur_gallery.activity.ImgurView;
import com.devbox.project.imgur_gallery.model.Gallery;
import com.devbox.project.imgur_gallery.viewmodel.GalleryViewModel;
import com.devbox.project.imgur_gallery.viewmodel.SingleGalleryViewModel;

import java.util.List;

/**
 * Created by alex on 17/11/17.
 */

public class ImgurPresenterImpl implements ImgurPresenter {

    private final ImgurInteractor interactor;;
    private final ImgurView view;

    public ImgurPresenterImpl(ImgurInteractor interactor, ImgurView view) {
        this.interactor = interactor;
        this.view = view;
    }

    @Override
    public void load(String text) {
        try {
            List<Gallery> list = interactor.searchGallery(text);
            final GalleryViewModel viewModel = new GalleryViewModel(list);
            view.displayGallery(viewModel);
        } catch (ImgurNotFoundException e) {
            view.displayNotFound();
        } catch (ImgurException e) {
            view.displayError();
        }
    }

    @Override
    public void loadById(String hash) {
        try {
            Gallery gallery = interactor.getGalleryByAlbumId(hash);
            final SingleGalleryViewModel viewModel = new SingleGalleryViewModel(
                    gallery.getTitle(),
                    gallery.getImages(),
                    gallery.getAccount_url(),
                    gallery.getDatetime(),
                    gallery.getDescription(),
                    gallery.getId(),
                    gallery.getImages_count()
            );
            view.displaySingleGallery(viewModel);
        } catch (ImgurNotFoundException  e) {
            view.displayNotFound();
        } catch (ImgurException e) {
            view.displayError();
        }
    }


}
