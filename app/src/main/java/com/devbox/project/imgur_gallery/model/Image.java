package com.devbox.project.imgur_gallery.model;

/**
 * Created by alex on 17/11/17.
 */

public class Image {
    private String link;

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
