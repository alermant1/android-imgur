package com.devbox.project.imgur_gallery.ImgurModule.dagger;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.devbox.project.imgur_gallery.ImgurModule.interactor.ImgurInteractor;
import com.devbox.project.imgur_gallery.ImgurModule.presenter.ImgurPresenter;
import com.devbox.project.imgur_gallery.ImgurModule.presenter.ImgurPresenterImpl;
import com.devbox.project.imgur_gallery.ImgurModule.repository.ImgurRepository;
import com.devbox.project.imgur_gallery.ImgurModule.repository.ImgurRepositoryImpl;
import com.devbox.project.imgur_gallery.activity.ImgurView;
import com.devbox.project.imgur_gallery.dagger.MainModule;
import com.devbox.project.imgur_gallery.helpers.Toaster;
import com.devbox.project.imgur_gallery.model.Gallery;
import com.devbox.project.imgur_gallery.viewmodel.GalleryViewModel;
import com.devbox.project.imgur_gallery.viewmodel.SingleGalleryViewModel;

import java.util.List;
import java.util.concurrent.Executor;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by alex on 17/11/17.
 */

@Module
public class ImgurModule {
    private final ImgurView view;

    public ImgurModule(ImgurView view) {
        this.view = view;
    }

    @Provides
    ImgurView providesImgurView(@Named(MainModule.FRONT) Executor executor) {
        return new ImgurViewDecorator(executor, view);
    }

    @Provides
    Toaster provideToaster(AppCompatActivity context) {
        return new Toaster(context);
    }

    @Provides
    ImgurPresenter providesImgurPresenter(
            @Named(MainModule.BACKGROUND) Executor executor,
            ImgurInteractor interactor,
            ImgurView view
    ) {
        return new ImgurPresenterDecorator(executor, new ImgurPresenterImpl(interactor, view));
    }

    @Provides
    ImgurInteractor providesImgurInteractor(ImgurRepository repository) {
        return new ImgurInteractor(repository);
    }

    @Provides
    ImgurRepository providesMyRepository() {
        return new ImgurRepositoryImpl();
    }

    class ImgurViewDecorator implements ImgurView {

        private Executor executor;
        private ImgurView view;

        ImgurViewDecorator(Executor executor, ImgurView view) {
            this.executor = executor;
            this.view = view;
        }

        @Override
        public void displayGallery(final GalleryViewModel viewModel) {
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    view.displayGallery(viewModel);
                }
            });
        }

        @Override
        public void displayNotFound() {
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    view.displayNotFound();
                }
            });
        }

        @Override
        public void displayError() {
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    view.displayError();
                }
            });
        }

        @Override
        public void displaySingleGallery(final SingleGalleryViewModel viewModel) {
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    view.displaySingleGallery(viewModel);
                }
            });
        }
    }

    class ImgurPresenterDecorator implements ImgurPresenter {

        private Executor executor;
        private ImgurPresenter presenter;

        ImgurPresenterDecorator(Executor executor, ImgurPresenter presenter) {
            this.executor = executor;
            this.presenter = presenter;
        }

        @Override
        public void load(final String text) {
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    presenter.load(text);
                }
            });
        }

        @Override
        public void loadById(final String hash) {
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    presenter.loadById(hash);
                }
            });

        }
    }
}
