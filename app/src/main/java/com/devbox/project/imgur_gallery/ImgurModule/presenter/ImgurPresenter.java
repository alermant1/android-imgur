package com.devbox.project.imgur_gallery.ImgurModule.presenter;

/**
 * Created by alex on 17/11/17.
 */

public interface ImgurPresenter {
    void load(String text);
    void loadById(String hash);
}
