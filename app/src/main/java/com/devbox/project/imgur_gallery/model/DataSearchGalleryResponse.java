package com.devbox.project.imgur_gallery.model;

import java.util.List;

/**
 * Created by alex on 17/11/17.
 */


/**
 * Cette classe permet au moment de la désérialization
 * de récupérer les données de la propriété
 * data qui contient une liste de Gallery
 */
public class DataSearchGalleryResponse {
    private List<Gallery> data;

    public List<Gallery> getData() {
        return data;
    }

    public void setData(List<Gallery> data) {
        this.data = data;
    }
}
