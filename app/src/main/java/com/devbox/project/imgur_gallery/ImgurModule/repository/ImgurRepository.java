package com.devbox.project.imgur_gallery.ImgurModule.repository;

import com.devbox.project.imgur_gallery.ImgurModule.exception.ImgurException;
import com.devbox.project.imgur_gallery.ImgurModule.exception.ImgurNotFoundException;
import com.devbox.project.imgur_gallery.model.Gallery;

import java.util.List;

/**
 * Created by alex on 17/11/17.
 */

public interface ImgurRepository {
    List<Gallery> searchGallery(String text) throws ImgurException, ImgurNotFoundException;
    Gallery getGalleryByAlbumId(String hash) throws ImgurException, ImgurNotFoundException;
}
