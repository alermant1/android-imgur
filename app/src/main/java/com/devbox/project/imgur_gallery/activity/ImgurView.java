package com.devbox.project.imgur_gallery.activity;

import com.devbox.project.imgur_gallery.viewmodel.GalleryViewModel;
import com.devbox.project.imgur_gallery.viewmodel.SingleGalleryViewModel;

/**
 * Created by alex on 17/11/17.
 */

public interface ImgurView {
    void displayGallery(GalleryViewModel viewModel);
    void displayNotFound();
    void displayError();
    void displaySingleGallery(SingleGalleryViewModel viewModel);
}
