package com.devbox.project.imgur_gallery.viewmodel;

import com.devbox.project.imgur_gallery.model.Image;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by alex on 17/11/17.
 */

/**
 * SingleGalleryViewModel est le détail d'une gallery
 */
public class SingleGalleryViewModel {
    private String title;
    private List<Image> images;
    private String accountName;
    private String dateTime;
    private String description;
    private String id;
    private String images_count;


    public SingleGalleryViewModel(String title, List<Image> images, String account_url, String datetime, String description, String id, String images_count) {
        this.title = title;
        this.images = images;
        this.accountName = account_url;
        this.dateTime = datetime;
        this.description = description;
        this.id = id;
        this.images_count = images_count;
    }


    public String getImages_count() {
        return images_count;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public List<Image> getImages() {
        return images;
    }

    public String getAccountName() {
        return accountName;
    }

    public String getDateTime() {
        // on convertit la date qui est au format string timestamp
        // en date au format DD-MM-YYYY
        // On * 1000 car le timestamp doit être en milliseconde et non en seconde
        Date date = new Date(Long.parseLong(this.dateTime) * 1000);
        Format format = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss");
        return format.format(date);
    }

    public String getDescription() {
        return description;
    }
}
