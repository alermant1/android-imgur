package com.devbox.project.imgur_gallery.ImgurModule.interactor;

import com.devbox.project.imgur_gallery.ImgurModule.exception.ImgurException;
import com.devbox.project.imgur_gallery.ImgurModule.exception.ImgurNotFoundException;
import com.devbox.project.imgur_gallery.ImgurModule.repository.ImgurRepository;
import com.devbox.project.imgur_gallery.model.Gallery;

import java.util.List;

/**
 * Created by alex on 17/11/17.
 */

public class ImgurInteractor {
    private ImgurRepository repository;

    public ImgurInteractor(ImgurRepository repository) {
        this.repository = repository;
    }

    public List<Gallery> searchGallery(String text) throws ImgurException, ImgurNotFoundException {
        return repository.searchGallery(text);
    }

    public Gallery getGalleryByAlbumId(String hash) throws ImgurException, ImgurNotFoundException {
        return repository.getGalleryByAlbumId(hash);
    }
}
