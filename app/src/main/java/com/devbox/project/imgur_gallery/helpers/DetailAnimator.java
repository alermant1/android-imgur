package com.devbox.project.imgur_gallery.helpers;


import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;

/**
 * Created by alex on 18/11/17.
 */

public class DetailAnimator {

    private View view;

    public DetailAnimator(View v) {
        this.view = v;
    }

    /**
     * Cache la view et attend 3000 ms
     * avant d'appeler le callback
     * @param runnable
     */
    public void hideDetail(Runnable runnable) {
        view.animate()
                .alpha(0)
                .setDuration(1000)
                .rotationBy(360)
                .setDuration(2000)
                .setInterpolator(new AccelerateDecelerateInterpolator());

        view.postDelayed(runnable, 3000);
    }

    /**
     * Affiche à nouveau la view
     */
    public void showDetail() {
        view.animate()
                .rotationBy(0)
                .alpha(100)
                .setDuration(2000)
                .setInterpolator(new AccelerateDecelerateInterpolator());
    }

}
