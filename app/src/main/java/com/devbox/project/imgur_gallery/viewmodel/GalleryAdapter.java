package com.devbox.project.imgur_gallery.viewmodel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.devbox.project.imgur_gallery.R;
import com.devbox.project.imgur_gallery.helpers.GalleryOnClickListener;
import com.devbox.project.imgur_gallery.helpers.GalleryOnClickListenerTablet;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;

/**
 * Created by alex on 17/11/17.
 */

/**
 * Utilisé pour afficher une liste d'item de gallerie
 */
public class GalleryAdapter extends BaseAdapter {

    private final Context context;
    private final List<SingleGalleryViewModel> listItem;
    public GalleryAdapter(Context context, List<SingleGalleryViewModel> listItem) {
        this.context = context;
        this.listItem = listItem;
    }

    @Override
    public int getCount() {
        return listItem.size();
    }

    @Override
    public Object getItem(int i) {
        return listItem.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup parent) {

        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.item, null);
        ImageView imageView = rowView.findViewById(R.id.imageView);
        TextView description = rowView.findViewById(R.id.description);
        TextView author = rowView.findViewById(R.id.author);
        TextView date = rowView.findViewById(R.id.date);
        TextView nbImage = rowView.findViewById(R.id.nbImage);

        nbImage.setText("(" + listItem.get(i).getImages_count() + " " + context.getResources().getString(R.string.images) +")");
        LinearLayout item = rowView.findViewById(R.id.itemView);

        boolean tabletSize = context.getResources().getBoolean(R.bool.isTablet);
        // sur tablet on affiche les infos sur le coté
        if (tabletSize) {
            item.setOnClickListener(new GalleryOnClickListenerTablet(context, listItem.get(i)));
        } else {
            // sur petit écran (téléphone) on affiche une nouvelle activity
            item.setOnClickListener(new GalleryOnClickListener(context, listItem.get(i)));
        }

        description.setText(listItem.get(i).getDescription());
        author.setText(listItem.get(i).getAccountName());
        date.setText(listItem.get(i).getDateTime());

        Picasso.with(context)
                .load(listItem.get(i).getImages().get(0).getLink())
                .placeholder(R.drawable.loading)
                .resize(400, 0)
                .into(imageView);

        return rowView;
    }

}
