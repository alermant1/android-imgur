package com.devbox.project.imgur_gallery.helpers;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;

import com.devbox.project.imgur_gallery.R;
import com.devbox.project.imgur_gallery.activity.MainActivity;
import com.devbox.project.imgur_gallery.viewmodel.DetailGalleryAdapter;
import com.devbox.project.imgur_gallery.viewmodel.SingleGalleryViewModel;

/**
 * Created by alex on 18/11/17.
 */

/**
 * Sur tablet quand on click sur un item, on affiche le résultat sur la partie droite
 * de l'écran, sur la même activity
 */
public class GalleryOnClickListenerTablet implements View.OnClickListener {

    private SingleGalleryViewModel galleryViewModel;
    private MainActivity context;

    public GalleryOnClickListenerTablet(Context context, SingleGalleryViewModel galleryViewModel) {
        this.galleryViewModel = galleryViewModel;
        this.context = (MainActivity) context;
    }

    @Override
    public void onClick(View view) {
        context.getPresenter().loadById(galleryViewModel.getId());
    }
}
