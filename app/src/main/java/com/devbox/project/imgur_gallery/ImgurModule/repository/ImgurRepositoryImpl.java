package com.devbox.project.imgur_gallery.ImgurModule.repository;

import com.devbox.project.imgur_gallery.ImgurModule.exception.ImgurException;
import com.devbox.project.imgur_gallery.ImgurModule.exception.ImgurNotFoundException;
import com.devbox.project.imgur_gallery.ImgurModule.service.ImgurService;
import com.devbox.project.imgur_gallery.model.DataGetGalleryResponse;
import com.devbox.project.imgur_gallery.model.DataSearchGalleryResponse;
import com.devbox.project.imgur_gallery.model.Gallery;

import java.io.IOException;
import java.util.List;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by alex on 17/11/17.
 */

public class ImgurRepositoryImpl implements ImgurRepository {
    private static final String BASE_URL = "https://api.imgur.com/3/";
    private ImgurService service;

    public ImgurRepositoryImpl() {
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build();
        this.service = retrofit.create(ImgurService.class);
    }

    @Override
    public List<Gallery> searchGallery(String text) throws ImgurException, ImgurNotFoundException {
        try {
            // on récupère la propriété "dataSearchGalleryResponse" qui contient
            // tout
            DataSearchGalleryResponse dataSearchGalleryResponse = service.searchGallery(text).execute().body();

            if (dataSearchGalleryResponse == null) throw new ImgurNotFoundException();
            if (dataSearchGalleryResponse.getData().size() == 0) throw new ImgurNotFoundException();
            return dataSearchGalleryResponse.getData();
        } catch (IOException e) {

            throw new ImgurException();
        }
    }

    @Override
    public Gallery getGalleryByAlbumId(String hash) throws ImgurException, ImgurNotFoundException {
        try {
            DataGetGalleryResponse dataGetGalleryResponse = service.getGalleryByAlbumId(hash).execute().body();
            if (dataGetGalleryResponse == null) throw new ImgurNotFoundException();
            Gallery gallery = (Gallery) dataGetGalleryResponse.getData();
            return gallery;
        } catch (IOException e) {
            throw new ImgurException();
        }
    }
}
