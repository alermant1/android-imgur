package com.devbox.project.imgur_gallery;

import android.app.Application;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by alex on 17/11/17.
 */

/**
 * Permet de mettre la même font à toutes les activity
 */
public class InitApplication extends Application {
    @Override
    public void onCreate(){
        super.onCreate();
        CalligraphyConfig.initDefault(
                new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/custom.otf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );
    }
}
