package com.devbox.project.imgur_gallery.helpers;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by alex on 18/11/17.
 */

public class Toaster {

    private Context context;

    public Toaster(Context context) {
        this.context = context;
    }

    public void toast(String text) {
        Toast.makeText(
                context,
                text,
                Toast.LENGTH_LONG
        ).show();
    }
}
