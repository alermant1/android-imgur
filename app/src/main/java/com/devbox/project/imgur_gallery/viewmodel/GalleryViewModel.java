package com.devbox.project.imgur_gallery.viewmodel;

import com.devbox.project.imgur_gallery.model.Gallery;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alex on 17/11/17.
 */

/**
 * GalleryViewModel est le résultat d'une recherche
 */
public class GalleryViewModel {

    private final List<SingleGalleryViewModel> listGallery;

    public GalleryViewModel(List<Gallery> listGallery) {

        this.listGallery = new ArrayList<SingleGalleryViewModel>();

        // on ne conserve que les gallery ayant au moins une image
        for(Gallery gallery : listGallery) {

            if (gallery.getImages() != null) {
                this.listGallery.add(new SingleGalleryViewModel(
                        gallery.getTitle(),
                        gallery.getImages(),
                        gallery.getAccount_url(),
                        gallery.getDatetime(),
                        gallery.getDescription(),
                        gallery.getId(),
                        gallery.getImages_count()
                        )
                );
            }
        }
    }

    public List<SingleGalleryViewModel> getAllGallery() {
        return listGallery;
    }
}
