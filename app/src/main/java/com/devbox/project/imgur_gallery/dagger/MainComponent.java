package com.devbox.project.imgur_gallery.dagger;

import com.devbox.project.imgur_gallery.ImgurModule.dagger.ImgurComponent;
import com.devbox.project.imgur_gallery.ImgurModule.dagger.ImgurModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by alex on 17/11/17.
 */

@Component(modules = MainModule.class)
@Singleton
public interface MainComponent {
    ImgurComponent plus(ImgurModule module);
}
