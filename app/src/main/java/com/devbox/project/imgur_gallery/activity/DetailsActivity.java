package com.devbox.project.imgur_gallery.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.devbox.project.imgur_gallery.ImgurModule.dagger.ImgurModule;
import com.devbox.project.imgur_gallery.ImgurModule.presenter.ImgurPresenter;
import com.devbox.project.imgur_gallery.R;
import com.devbox.project.imgur_gallery.dagger.DaggerMainComponent;
import com.devbox.project.imgur_gallery.helpers.Toaster;
import com.devbox.project.imgur_gallery.viewmodel.DetailGalleryAdapter;
import com.devbox.project.imgur_gallery.viewmodel.GalleryViewModel;
import com.devbox.project.imgur_gallery.viewmodel.SingleGalleryViewModel;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by xps on 17/11/17.
 */

public class DetailsActivity  extends BaseActivity implements ImgurView{

    @Inject
    ImgurPresenter presenter;

    @BindView(R.id.gridDetailView)
    GridView gridView;
    @BindView(R.id.titreDetail)
    TextView titreDetail;
    @BindView(R.id.auteurDetail)
    TextView auteurDetail;
    @BindView(R.id.dateDetail)
    TextView dateDetail;
    @BindView(R.id.descriptionDetail)
    TextView descriptionDetail;

    Toaster toaster;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details);

        ButterKnife.bind(this);
        DaggerMainComponent.builder()
                .build()
                .plus(new ImgurModule(this))
                .injectDetail(this);

        toaster = new Toaster(this);

        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        // on récupère l'id de la gallery
        String hash = (String) bundle.getSerializable("hash");
        // on récupère les données de la gallerie
        presenter.loadById(hash);

    }

    /**
     * Overide le button back en haut à gauche
     * pour ne pas recréer l'activity MainActivity
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void displayGallery(GalleryViewModel viewModel) {
        // on ne cherche pas de GalleryViewModel dans cette activity
    }

    @Override
    public void displayNotFound() {
        toaster.toast(getResources().getString(R.string.galleryError));
    }

    @Override
    public void displayError() {
        toaster.toast(getResources().getString(R.string.ImgurError));
    }

    /**
     * Callback appelé quand le WS a finit son retour et les données
     * sont prêtes à etre afficher
     * @param viewModel
     */
    @Override
    public void displaySingleGallery(SingleGalleryViewModel viewModel) {
        titreDetail.setText(viewModel.getTitle());
        auteurDetail.setText(getResources().getString(R.string.postedBy) + " " + viewModel.getAccountName());
        dateDetail.setText(viewModel.getDateTime());
        descriptionDetail.setText(viewModel.getDescription());

        gridView.setAdapter(new DetailGalleryAdapter(this, viewModel));

    }
}
